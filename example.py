# -*- coding:utf-8 -*-

import urllib.request,sys
from bs4 import BeautifulSoup

URL="http://img.177pic.info/uploads/2016/06a/00576.jpg"
USERAGENT = "Mozilla/5.0"

def progress(block_count,block_size,total_size):
    percentage = 100.0 * block_count * block_size / total_size
    sys.stdout.write('%.2f %% ( %d KB)\r' % (percentage,total_size/1024))

def making_soup(url):
    req = urllib.request.Request(url)
    req.add_header('User-agent',USERAGENT)
    res = urllib.request.urlopen(req)
    html = res.read()
    soup = BeautifulSoup(html,'html.parser')
    return soup

if __name__ == "__main__":
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent','Mozila/5.0')]
    urllib.request.install_opener(opener)
    urllib.request.urlretrieve(URL,"movie3.jpg",progress)
    """
    # urlretrieveと等価処理 #
    req = urllib.request.Request(URL)
    req.add_header('User-agent',USERAGENT)
    with urllib.request.urlopen(req) as response:
        with open("test.jpg","w") as f:
            f.write(response)
    """

