
import urllib.request,sys,time
from bs4 import BeautifulSoup

URL="http://javcuteonline.com/"
USERAGENT = "Mozilla/5.0"

# 進捗報告
def progress(block_count,block_size,total_size):    
    percentage = 100.0 * block_count * block_size / total_size
    sys.stdout.write('%.2f %% ( %d KB)\r' % (percentage,total_size/1024))


# スープ作り
def making_soup(url):
    req = urllib.request.Request(url)
    req.add_header('User-agent',USERAGENT)
    res = urllib.request.urlopen(req)
    html = res.read()
    soup = BeautifulSoup(html,'html.parser')
    return soup


# Referer付きスープ作り
def making_soup2(url):
    req = urllib.request.Request(url)
    req.add_header('User-agent',USERAGENT)
    req.add_header('Referer','http://javcuteonline.com/kawd-730-%e7%b5%b6%e5%80%ab%e3%83%87%e3%82%ab%e3%83%81%e3%83%b3%e7%94%b7%e3%81%ae%e7%b2%be%e5%ad%90%e3%81%8c%e5%b0%bd%e3%81%8d%e6%9e%9c%e3%81%a6%e3%82%8b%e3%81%be%e3%81%a7%e7%b9%b0%e3%82%8a%e8%bf%94/')
    res = urllib.request.urlopen(req)
    html = res.read()
    soup = BeautifulSoup(html,'html.parser')
    return soup


# 動画のURLを取り出す
def extract_each_url(url):
    for title in url.findAll('h3'):
        for movie in title.findAll('a'):
            movie_url = dict(movie.attrs)['href']
            handle_each_url(movie_url)


# 取り出したURLに対して処理をする
def handle_each_url(url):
    html = making_soup(url)
    for item_content in html.findAll('div',attrs={'class':'item-content'}):
        for iframe_url in item_content.findAll('iframe'):
            # iframe中のHTMLをリクエスト
            query_html(dict(iframe_url.attrs)['src'])


# iframe中のHTMLをリクエスト
def query_html(url):
    # iframe中のHTMLに対してはReferer付きでスープ作り
    html = making_soup2(url)
    for video_tag in html.findAll('video'):
        print (video_tag)
        parts_url = (dict(video_tag.attrs)['poster'].split('/')[2])
        parts_url2 = (dict(video_tag.attrs)['poster'].split('/')[-1].split(".")[0])
        for src in video_tag.findAll('source'):
            parts_url3 = (dict(src.attrs)['src'].split("/")[-1].split(".")[-1])
            movie_url = ("http://{}/v1/{}.{}".format(parts_url,parts_url2,parts_url3))
            download(movie_url,url)
            exit()

# ダウンロード
def download(content,referer):
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent','Mozila/5.0'),('Referer',referer)]
    print ("Referer: {}".format(referer))
    print ("Content: {}".format(content))
    urllib.request.install_opener(opener)
    urllib.request.urlretrieve(content,"test.mp4",progress)


# main関数
if __name__ == "__main__":
	html = making_soup(URL)
	extract_each_url(html)


"""
base_url = http://ack.cdn.vizplay.org/v1/2/b5ef57919b73200c744121a8e358b69f.mp4?st=gfnGzA0x6g5g1pGQury6Qw&amp;hash=4Z9C2nqb9DtF64Xh3JALvw
video_url = http://aa6.cdn.vizplay.org/v1/a6b97e150d9062174e9797ba01335c09.mp4?st=m5Bpu-JYnoyDpWg7mgCTiA&hash=QtOC8Ce0IzSJolNVdvUH2g
jpg_url = http://abd.cdn.vizplay.org/videos/screenshots/c94e22858dfb0c8d9af7c6bf89393a6e.jpg
"""


